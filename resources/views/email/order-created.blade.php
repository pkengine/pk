@component('mail::message')
<h1 style="text-align: center">Order #{{ $order->id }}</h1>
@component('mail::table')
|       | Title | Price |
|:------|:-----|------:|
@foreach($order->items as $item)
| <div style="width: 80px;"><img src="{{ Arr::get($item, 'image_url_thumb_p') }}" width="60px"></div> | {{ Arr::get($item, 'title') }} | <div style="width: 80px;">{{ Arr::get($item, 'price') }} x {{ Arr::get($item, 'count') }}</div> |
@endforeach
@endcomponent

<h4>Total: {{ $order->price }}</h4>

@component('mail::panel')
    <p>Contact phone: {{ $order->phone }}</p>
    <p>Delivery address: {{ $order->address }}</p>
    <p>Delivery datetime: {{ $order->atTz('delivery_datetime', '%e %b %Y, %T') }}</p>
@endcomponent

@endcomponent
