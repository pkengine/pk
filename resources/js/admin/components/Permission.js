export default {
    install(Vue) {
        function hasPermission(permission) {
            if(_.has(this.$store.getters.user, 'role.permissions')){

                return this.$store.getters.user.is_owner || (_.indexOf(this.$store.getters.user.role.permissions, permission) != -1)
            }
            return true //todo
            return false
        }
        Vue.hasPermission = Vue.prototype.$hasPermission = hasPermission
    }
}
