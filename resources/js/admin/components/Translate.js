export default {
    install(Vue) {
        function t(key, params, defaultValue) {
            if(typeof defaultValue !== 'underfined' && !_.has(this.$vuetify.lang.locales[this.$vuetify.lang.current], key)){
                return defaultValue
            }
            return this.$vuetify.lang.t('$vuetify.'+ key, params);
        }
        Vue.t = Vue.prototype.$t = t
    }
}
