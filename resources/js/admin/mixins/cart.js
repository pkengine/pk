import _ from "lodash";

export default {
    props: ['items', 'count', 'productsPrice', 'totalPrice'],
    data() {
        return {
            isQuery: false,
            cart: {
                items: {},
                products: [],
                count: 0,
                total_count: 0,
                total_price: 0,
                is_empty: true
            },
            details:{
                phone: '',
                email: '',
                address: '',
                delivery_datetime: null,
            },
            errors: {},
            order: null,
            appEmitReady: false
        }
    },
    watch:{
        cart:{
            handler(cart){
                this.$emit('update:items', cart.items)
                this.$emit('update:count', cart.count)
                this.$emit('update:totalPrice', cart.total_price)
                this.$emit('update:productsPrice', cart.products_price)
            },
            deep: true
        }
    },
    methods:{
        addItem(id, count){
            if(this.isQuery) return false
            count = _.max([_.toInteger(count), 1])
            axios.post('/cart/add/' + id, {params: {count: count}})
                .then((response) => {
                    if(_.isObject(response.data)){
                        this.cart = response.data
                    }
                });
        },
        subItem(id, count){
            if(this.isQuery) return false
            count = _.max([_.toInteger(count), 1])
            axios.post('/cart/sub/' + id, {params: {count: count}})
                .then((response) => {
                    if(_.isObject(response.data)){
                        this.cart = response.data
                    }
                });
        },
        removeItem(id, count){
            if(this.isQuery) return false
            axios.post('/cart/remove/' + id)
                .then((response) => {
                    if(_.isObject(response.data)){
                        this.cart = response.data
                    }
                });
        },
        submit(){
            if(this.isQuery) return false
            this.isQuery = true
            this.errors = {}
            axios.post('/order', this.details)
                .then((response) => {
                    if(response.data.success){
                        this.cart = response.data.cart
                        this.$emit('order-success', response.data.order)
                    }
                })
                .catch((error) => {
                    this.validatorErrors(error);
                })
                .finally(() => {
                    this.isQuery = false
                })
        },
        validatorErrors(error, prefix) {
            if (_.has(error, 'response.data.errors')) {
                let errors = _.mapValues(_.get(error, 'response.data.errors', {}), 0);
                if(prefix){
                    this.errors[prefix] = errors
                }else{
                    this.errors = errors
                }
            }
        },
        addEmitListener(){
            this.appEmitReady = true
            window.$app.$on('addCartItem',  (id) => {
                this.addItem(id)
            })
            window.$app.$on('subCartItem', (id) => {
                this.subItem(id)
            })
            window.$app.$on('removeCartItem', (id) => {
                this.removeItem(id)
            })
        }

    },
    mounted() {
    },
    created() {
        axios.get('/cart')
            .then((response) => {
                if(_.isObject(response.data)){
                    this.cart = response.data
                }
            })
        if(window.$app){
            this.addEmitListener()
        }else{
            document.addEventListener('DOMContentLoaded', () => {
                console.log('emits')
                let x = 0;
                let intervalListener = setInterval(() => {
                    if(window.$app){
                        this.addEmitListener()
                        clearInterval(intervalListener)
                    }else if(x > 20){ clearInterval(intervalListener)}
                }, 500)

            })
        }

    }
}
