export var post = {
    data(){
      return {
          type: 'post'
      }
    },
    computed:{
        template(){
            return 'post-edit'
        },
        templateCategory(){
            return this.editModel.category ? 'post-' + this.editModel.category.slug + '-edit' : false
        },
        templateSidebar(){
            return 'post-edit-sidebar'
        },
        templateCategorySidebar(){
            return this.editModel.category ? 'post-' + this.editModel.category.slug + '-sidebar-edit' : false
        },

        hasTemplate(){
            return this.hasComponent(this.template)
        },
        hasTemplateCategory(){
            return this.templateCategory ? this.hasComponent(this.templateCategory) : false
        },
        hasTemplateSidebar(){
            return this.hasComponent(this.templateSidebar)
        },
        hasTemplateCategorySidebar(){
            return this.templateCategorySidebar ? this.hasComponent(this.templateCategorySidebar) : false
        }
    },
}
export var product = {
    data() {
        return {
            type: 'product'
        }
    },
    mixins: [post]
}

export var order = {
    data() {
        return {
            type: 'order'
        }
    },
    mixins: [post]
}

export var page = {
    computed: {
        template() {
            return 'page-edit'
        },
        templateTemplate() {
            return this.editModel.category ? 'page-' + this.editModel.template + '-edit' : false
        },
        templateSidebar() {
            return 'page-sidebar-edit'
        },
        templateTemplateSidebar() {
            return this.editModel.template ? 'page-' + this.editModel.template + '-sidebar-edit' : false
        },


        hasTemplate(){
            return this.hasComponent(this.template)
        },
        hasTemplateTemplate(){
            return this.templateTemplate ? this.hasComponent(this.templateTemplate) : false
        },
        hasTemplateSidebar(){
            return this.hasComponent(this.templateSidebar)
        },
        hasTemplateTemplateSidebar(){
            return this.templateTemplateSidebar ? this.hasComponent(this.templateTemplateSidebar) : false
        }
    }
}

export var category = {
    computed: {
        template() {
            return 'category-edit'
        },
        templateSidebar() {
            return 'category-sidebar-edit'
        },
        hasTemplate(){
            return this.hasComponent(this.template)
        },
        hasTemplateSidebar(){
            return this.hasComponent(this.templateSidebar)
        },
    }
}

export var tag = {
    computed: {
        template() {
            return 'category-edit'
        },
        templateSidebar() {
            return 'category-sidebar-edit'
        },
        hasTemplate(){
            return this.hasComponent(this.template)
        },
        hasTemplateSidebar(){
            return this.hasComponent(this.templateSidebar)
        },
    }
}
