import Vue from 'vue'
import VueRouter from 'vue-router'

import Index from './../Index'
import Account from './../pages/Account'
import Settings from './../pages/Settings'

import Roles from './../pages/Roles'

import Pages from './../pages/Pages'
import EditPage from './../edit/EditPage'

import Posts from './../pages/Posts'
import EditPost from './../edit/EditPost'

import Users from './../pages/Users'
import Images from './../pages/Images'

import Categories from './../pages/Categories'
import EditCategory from './../edit/EditCategory'

import Tags from './../pages/Tags'
import EditTag from './../edit/EditTag'

import Products from './../pages/Products'
import EditProduct from './../edit/EditProduct'

import Orders from './../pages/Orders'
import EditOrder from './../edit/EditOrder'

Vue.use(VueRouter)

let routes = [
    {path: '/', name: 'index', component: Index},
    {path: '/account', name: 'account', component: Account},
    {path: '/settings', name: 'settings', component: Settings},

    {path: '/roles', name: 'roles', component: Roles},
    {path: '/roles/:id', name: 'role', component: Roles},

    {path: '/pages', name: 'pages', component: Pages},
    {path: '/pages/create', name: 'page-create', component: EditPage},
    {path: '/pages/:id', name: 'page', component: EditPage},

    {path: '/posts', name: 'posts', component: Posts},
    {path: '/posts/create', name: 'post-create', component: EditPost},
    {path: '/posts/:id', name: 'post', component: EditPost},
    {path: '/users', name: 'users', component: Users},
    {path: '/users/:id', name: 'user', component: Users},
    {path: '/categories', name: 'categories', component: Categories},
    {path: '/categories/create', name: 'category-create', component: EditCategory},
    {path: '/categories/:id', name: 'category', component: EditCategory},
    {path: '/tags', name: 'tags', component: Tags},
    {path: '/tags/create', name: 'tag-create', component: EditTag},
    {path: '/tags/:id', name: 'tag', component: EditTag},
    {path: '/images', name: 'images', component: Images},

    {path: '/products', name: 'products', component: Products},
    {path: '/products/create', name: 'product-create', component: EditProduct},
    {path: '/products/:id', name: 'product', component: EditProduct},

    {path: '/orders', name: 'orders', component: Orders},
    {path: '/orders/:id', name: 'order', component: EditOrder},
];

export default  new VueRouter({
    routes
});
