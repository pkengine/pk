import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

const opts = {
    icons: {
        iconfont: 'fa',
    },
}

const vuetify = new Vuetify(opts)

export default vuetify
