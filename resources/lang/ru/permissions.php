<?php

return [
    'posts' => 'Записи',
    'posts.index' => 'Просмотр списка записей',
    'posts.create' => 'Создание записей',
    'posts.update' => 'Редактирование записей',
    'posts.updateMy' => 'Редактирование своих записей',
    'posts.delete' => 'Удаление записей',
    'posts.deleteMy' => 'Удаление своих записей',

    'pages' => 'Страницы',
    'pages.index' => 'Просмотр списка страниц',
    'pages.create' => 'Создание страниц',
    'pages.update' => 'Редактирование страниц',
    'pages.delete' => 'Удаление страниц',

    'categories' => 'Категории',
    'categories.index' => 'Просмотр списка категорий',
    'categories.create' => 'Создание категорий',
    'categories.update' => 'Редактирование категорий',
    'categories.delete' => 'Удаление категорий',

    'taxonomies' => 'Таксономии',
    'taxonomies.index' => 'Просмотр списка таксономий',
    'taxonomies.create' => 'Создание таксономий',
    'taxonomies.update' => 'Редактирование таксономий',
    'taxonomies.delete' => 'Удаление таксономий',

    'tags' => 'Теги',
    'tags.index' => 'Просмотр списка тегов',
    'tags.create' => 'Создание тегов',
    'tags.update' => 'Редактирование тегов',
    'tags.delete' => 'Удаление тегов',

    'images' => 'Изображения',
    'images.create' => 'Загрузка изображений',
    'images.update' => 'Редактирование изображений',
    'images.delete' => 'Удаление изображений',

    'settings' => 'Настройки',
    'settings.update' => 'Редактирование настроек',

    'orders' => 'Заказы',
    'orders.index' => 'Просмотр списка заказов',
    'orders.update' => 'Редактирование заказов',
];
