<?php
namespace PK\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;

class MenuManager
{

    protected $items;

    public function __construct()
    {
        $this->items = collect();
    }

    /**
     * @param array $menu
     * @return array
     */
    public function get()
    {
        //$permissions = Auth::check() && Auth::user()->client->role ? Auth::user()->role->permissions : [];
        $permissions = [];
        return array_values($this->getItems($this->items->sortBy('sort')->toArray(), $permissions));
    }

    /**
     * @param array $items
     * @return MenuManager
     */
    public function set(array $items)
    {
        $this->items = $this->items->merge($items);
        return $this;
    }

    private function getItems($items, $permissions, $member = false)
    {
        foreach($items as $key => $item){
            if($member && !Auth::user()->isOwner()){
                if(isset($items[$key]['permission'])){
                    if(is_array($items[$key]['permission']) && count(array_intersect($items[$key]['permission'], $permissions)) === 0){
                        unset($items[$key]);
                        continue;
                    }elseif(is_string($items[$key]['permission']) && !in_array($items[$key]['permission'], $permissions)){
                        unset($items[$key]);
                        continue;
                    }

                }
            }

            if(($method = Arr::get($item, 'badge.function')) && method_exists(static::class, Arr::get($item, 'badge.function'))){
                $items[$key]['badge']['value'] = static::$method();
            }

            $items[$key]['title'] = __(Arr::get($item, 'title'));
            if(Arr::has($items[$key], 'items')){
                $items[$key]['active'] = false;
                $items[$key]['items'] = static::getItems($items[$key]['items'], $permissions, $member);
                if(!count($items[$key]['items'])){
                    unset($items[$key]);
                }
            }
        }
        return $items;
    }
}
