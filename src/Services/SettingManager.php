<?php


namespace PK\Services;


use Illuminate\Support\Facades\App;
use PK\Models\Setting;
use Illuminate\Support\Arr;

class SettingManager
{
    protected $settings = [];

    /**
     * @param string|Setting $name
     * @param bool $isSettings
     * @param bool $global
     * @return bool|mixed|null
     */
    public function get($name, $isSettings = false, $global = true)
    {
        $setting = null;

        if(is_string($name)){
            if(Arr::has($this->settings, $name)){
                $setting = Arr::get($this->settings, $name);
            }else{
                if($isSettings){
                    $setting = Setting::where('name', $name)->withoutGlobalscope('lang')->first();
                }else{
                    $setting = Setting::where('name', $name)->first();
                }

            }
        }elseif(is_object($name) && $name instanceof Setting){
            $setting = $name;
        }

        if(!$setting) return null;

        $return = null;

        $return = $this->prepareValue($setting->type, $setting->value, $setting->getAttribute('name'), $isSettings);

        if(!$isSettings && $global){
            if($setting instanceof Setting){
                $this->settings[$setting->getAttribute('name')] = $setting;
            }else{
                $this->settings[$name] = null;
            }
        }

        return $return ?? null;
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param string $type
     * @param string|null $lang
     * @return mixed
     */
    public function set(string $name, $value, $type = 'string', $lang = null)
    {
        if(in_array($name, $this->provider()->excludeLang)){
            $lang = null;
            Setting::withoutGlobalScope('lang')->where(['name' => $name])->whereNotNull('lang')->delete();
        }else{
            Setting::withoutGlobalScope('lang')->where(['name' => $name])->whereNull('lang')->delete();
        }
        if($type === 'relations'){
            if($class = Arr::get($this->provider()->relations, $name)){
                $value = $class::whereIn('id', (array)$value)->pluck('id')->toArray();
            }else{
                $value = [];
            }
        }
        if($type === 'relation'){
            if($class = Arr::get($this->provider()->relations, $name)){
                if($model = $class::where('id', (int)$value)->first()){
                    $value = $model->id;
                }else{
                    $value = null;
                }
            }else{
                $value = null;
            }
        }

        $data = [
            'value' => json_encode($value),
            'type' => $type
        ];
        return Setting::withoutGlobalScope('lang')->updateOrCreate(['name' => $name, 'lang' => $lang], $data);
    }

    /**
     * @param string|null $lang
     * @return array
     */
    public function all(string $lang = null)
    {
        $settings = collect($this->provider()->getSettings());

        $query = Setting::query();
        if($lang){
            $query = $query->withoutGlobalScope('lang')
                ->where('settingable_id', null)
                ->where('lang', $lang)
                ->orWhereNull('lang');
        }
        $settingsModels = $query->get()->keyBy('name');
        $settings = $settings->map(function ($type, $name) use ($settingsModels){
            if($settingsModels->has($name)){
                return $this->get($settingsModels->get($name), true);
            }else{
                return $this->prepareType($type);
            }
        });
        return $settings->put('lang', App::getLocale())->toArray();
    }

    /**
     * @param string $type
     * @param mixed $value
     * @param string|null $name
     * @param bool $isSettings
     * @return mixed
     */
    public function prepareValue(string $type, $value, $name = null, $isSettings = false)
    {
        $return = null;
        if($type == 'string' || !$type){
            $return = (string)$value;
        }elseif($type == 'boolean'){
            $return = (bool)$value;
        }elseif($type == 'float'){
            $return = (float)$value;
        }elseif($type == 'integer'){
            $return = (integer)$value;
        }elseif($type == 'array' || $type == 'object'){
            $return = $value;
        }elseif ($type == 'relation'){
            if($isSettings){
                $return = (int)$value;
            }elseif(class_exists($class = Arr::get($this->provider()->relations, $name))){
                $return = $class::find((int)$value);
            }
        }elseif ($type == 'relations'){
            if($isSettings){
                $return = (array)$value;
            }elseif(class_exists($class = Arr::get($this->provider()->relations, $name))){
                $return = $class::find((array)$value);
            }
        }
        return $return;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function prepareType(string $type = 'string')
    {
        if($type == 'boolean'){
            return false;
        }elseif($type == 'integer' || $type == 'float'){
            return 0;
        }elseif($type == 'array' || $type == 'relations'){
            return [];
        }elseif($type == 'object'){
            return new \stdClass();
        }elseif ($type == 'relation'){
            return null;
        }else{
            return '';
        }
    }

    /**
     * @return SettingsProvider
     */
    public function provider()
    {
        $settingsProviderClass = config('app.settingsProvider', \PK\Providers\SettingsProvider::class);

        return new $settingsProviderClass();
    }

    public function refresh()
    {
        $this->settings = [];
    }
}
