<?php


namespace PK\Services;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PK\Models\Page;
use PK\Models\Post;

class SitemapManager
{
    protected $sitemap;

    protected $model;

    protected $instance;

    const PRIORITY = [
        'post' => '0.8',
        'page' => '0.9',
    ];

    const INSTANCES = [
        'page', 'post', 'category', 'tag'
    ];

    public function __construct()
    {
        $this->sitemap = new \SimpleXMLElement("<?xml version='1.0' encoding='UTF-8' ?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" />");
    }

    public function generateMap()
    {
        foreach (static::INSTANCES as $instance){
            $this->setSitemap(route('app.sitemap', $instance));
        }
    }

    public function generate($instance)
    {

        $this->instance = $instance;
        $this->model = '\PK\Models\\'.Str::studly($instance);
        if(!class_exists($this->model)){
            abort(404);
        }
        $paginate = $this->model::paginate(10);
        $pages = $paginate->lastPage();
        $page = $paginate->currentPage();
        for($i = 1; $i <= $pages; $i++){
            if($i !== $page){
                if($i == 1){
                    $this->setSitemap(route('app.sitemap', ['instance' => $instance]));
                }else{
                    $this->setSitemap(route('app.sitemap', ['instance' => $instance, 'page' => $i]));
                }
            }
        }
        $paginate->map(function ($item) use ($instance) {
            $this->setUrl($item);
        });
    }

    protected function setUrl($item)
    {
        if(!$item->slug) return;
        try{
            if($route = route($this->instance, $item->slug)){
                $url = $this->sitemap->addChild('url');
                $url->addChild('loc', route($this->instance, $item->slug));
                $url->addChild('lastmod', $item->atTz('updated_at', 'c'));
                $url->addChild('changefreq', 'weekly');
                $url->addChild('priority',Arr::get(static::PRIORITY, $this->instance, '0.6'));
            }
        }catch (\Exception $e){}
    }

    protected function setSitemap($url)
    {
        $sitemap = $this->sitemap->addChild('sitemap');
        $sitemap->addChild('loc', $url);
    }

    public function getXML()
    {
        return $this->sitemap->asXML();
    }
}
