<?php

namespace PK\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $namespace = 'App';

    protected function sortBy($default = 'id')
    {
        $field = app('request')->get('sort_by');
        return in_array($field, static::SORTED) ? $field : $default;
    }

    public function returnPaginate($query, $model)
    {
        $class = 'PK\Resources\\'.$this->namespace.'\Collection';
        return response()->json($class::pagination($query->paginate(app('request')->get('per_page', 15)), $model));
    }
}
