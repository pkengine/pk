<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use PK\Resources\Admin\TagResource;
use PK\Models\MetaField;
use PK\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PK\Controllers\Controller;

class TagController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);
        $query = Tag::query();
        if($request->s){
            $query = $query->search(['title']);
        }

        return $this->returnPaginate($query, 'Tag');
    }

    public function make()
    {
        return response()->json(new TagResource(Tag::make()->load(Tag::FULL_RELATIONS)));
    }

    public function edit(Request $request, Tag $tag)
    {
        $tag->load(Tag::FULL_RELATIONS);
        return response()->json(new TagResource($tag));
    }

    public function show(Request $request, Tag $tag)
    {
        $tag->load(Tag::FULL_RELATIONS);
        return response()->json(new TagResource($tag));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'settings' => 'array'
        ], Tag::getValidate()));
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $tag = Tag::create($requestData);
        $tag->saveLangs($requestData['langs']);
        $tag->saveSettings(Arr::get($requestData,'settings', []));
        $tag->refresh();
        $tag->load(Tag::FULL_RELATIONS);
        return response()->json(new TagResource($tag));
    }


    /**
     * @param Request $request
     * @param Tag $tag
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Tag $tag)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'settings' => 'array'
        ], Tag::getValidate()));
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $tag->update($requestData);
        $tag->saveLangs($requestData['langs']);
        $tag->saveSettings(Arr::get($requestData,'settings', []));
        $tag->refresh();
        $tag->load(Tag::FULL_RELATIONS);
        return response()->json(new TagResource($tag));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy($tag)
    {
        $tag = Tag::withTrashed()->findOrFail($tag);
        $tag->langs()->delete();
        $tag->settings()->delete();
        $tag->delete();
        return response()->json(['success' => 1]);
    }

}
