<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use PK\Resources\Admin\PageResource;
use PK\Models\MetaField;
use PK\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PK\Controllers\Controller;

class PageController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);
        $query = Page::query();
        if($request->s){
            $query = $query->search(['title']);
        }

        return $this->returnPaginate($query, 'Page');
    }

    public function make()
    {
        return response()->json(new PageResource(Page::make()->load(Page::FULL_RELATIONS)));
    }

    public function show(Request $request, Page $page)
    {
        $page->load(Page::FULL_RELATIONS);
        return response()->json(new PageResource($page));
    }

    public function edit(Request $request, Page $page)
    {
        $page->load(Page::FULL_RELATIONS);
        return response()->json(new PageResource($page));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'template' => 'nullable|string|max:220',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'settings' => 'array'
        ], Page::getValidate()));
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $requestData['template'] = (string)Arr::get($requestData, 'template');
        $page = Page::create($requestData);
        $page->saveLangs($requestData['langs']);
        $page->saveSettings(Arr::get($requestData,'settings', []));
        $page->refresh();
        $page->load(Page::FULL_RELATIONS);
        return response()->json(new PageResource($page));
    }


    /**
     * @param Request $request
     * @param Page $page
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Page $page)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'template' => 'nullable|string|max:220',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'settings' => 'array'
        ], Page::getValidate()));

        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $requestData['template'] = (string)Arr::get($requestData, 'template');
        $page->update($requestData);
        $page->saveLangs($requestData['langs']);
        $page->saveSettings(Arr::get($requestData,'settings', []));
        $page->refresh();
        $page->load(Page::FULL_RELATIONS);
        return response()->json(new PageResource($page));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy($page)
    {
        $page = Page::findOrFail($page);
        $page->langs()->delete();
        $page->settings()->delete();
        $page->delete();
        return response()->json(['success' => 1]);
    }

}
