<?php

namespace PK\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use PK\Controllers\Controller;
use PK\Models\Order;
use PK\Resources\Admin\OrderResource;

class OrderController extends Controller
{
    public $namespace = 'Admin';

    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string',
        ]);

        $query = Order::query();
        return $this->returnPaginate($query, 'Order');
    }

    public function edit(Request $request, Order $order)
    {
        return response()->json(new OrderResource($order));
    }

    public function show(Request $request, Order $order)
    {
        return response()->json(new OrderResource($order));
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Order $order)
    {

        $requestData = $request->validate(array_merge([
            'status' => ['required', Rule::in(array_keys(Order::STATUSES))],
            'items.*.count' => 'required|integer',
            'items.*.id' => 'required|integer'
        ]));

        $mewItems = collect(Arr::get($requestData, 'items', []))->pluck('count', 'id');
        $items = collect($order->items)->map(function ($item) use ($mewItems){
            if(is_numeric($count = $mewItems->get(Arr::get($item, 'id', null)))){
                $item['count'] = (int)$count;
                $item['total_price'] = Arr::get($item, 'price') * $count;
            }
            return $item;
        });
        $requestData['price'] = $items->sum('total_price');
        $requestData['items'] = $items->toArray();
        $order->update($requestData);
        return response()->json(new OrderResource($order));
    }

    public function statuses()
    {
        $response = collect(Order::STATUSES)->map(function ($key, $item){
            return [
                'text' => __('pk::admin.order.status.'.$key),
                'value' => $item
            ];
        });
        return response()->json($response);
    }
}
