<?php


namespace PK\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use PK\Controllers\Controller;
use PK\Models\Role;
use PK\Resources\Admin\RoleResource;

class RoleController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);
        $query = Role::query();
        if($request->s){
            $query = $query->search(['title']);
        }

        return $this->returnPaginate($query, 'Role');
    }

    public function make()
    {
        return response()->json(new RoleResource(Role::make()));
    }

    public function show(Request $request, Role $role)
    {
        return response()->json(new RoleResource($role));
    }

    public function edit(Request $request, Role $role)
    {
        return response()->json(new RoleResource($role));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'permissions' => 'required|array',
            'permissions.*' => 'required|string',
        ]);
        $requestData['permissions'] = Arr::only($requestData['permissions'], app()->permissions);
        $role = Role::create($requestData);
        $role->saveLangs($requestData['langs']);
        $role->refresh();
        return response()->json(new RoleResource($role));
    }

    /**
     * @param Request $request
     * @param Role $role
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Role $role)
    {
        $requestData = $request->validate([
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'permissions' => 'required|array',
            'permissions.*' => 'required|string',
        ]);
        $requestData['permissions'] = Arr::only($requestData['permissions'], app()->permissions);
        $role->saveLangs($requestData['langs']);
        $role->update($requestData);
        $role->refresh();
        return response()->json(new RoleResource($role));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->langs()->delete();
        $role->delete();
        return response()->json(['success' => 1]);
    }

    public function permissions()
    {
        return response()->json(collect(app()->permissions)->map(function ($item){
            return [
                'text' => __('pk::permissions.'.$item),
                'value' => $item,
                'group' => Str::before($item, '.')
            ];
        })->groupBy('group')->map(function ($items, $group){
            return [
                'items' => $items,
                'group' => [
                    'text' => __('pk::permissions.'.$group),
                    'value' => $group,
                ]
            ];
        })->values());
    }
}
