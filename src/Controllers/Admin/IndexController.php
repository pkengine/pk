<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use PK\Facades\Langs;
use PK\Facades\Menu;
use PK\Controllers\Controller;
use PK\Models\Order;
use PK\Resources\Admin\UserResource;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin::index');
    }

    public function data()
    {
        $pageTemplates = collect(scandir(resource_path('/views/app/pages')))
            ->map(function ($item){
                $name = Str::before($item, '.');
                if($name){
                    return [
                        'text' => __('pages.'.$name),
                        'value' => $name
                    ];
                }
                return null;
            })->filter()->values();

        return response()->json([
            'user' => Auth::check() ? new UserResource(Auth::user()) : null,
            'menu' => $this->menu(),
            'pageTemplates' => collect(scandir(resource_path('/views/app/pages')))
                ->map(function ($item){
                    $name = Str::before($item, '.');
                    if($name){
                        return [
                            'text' => __('pages.'.$name),
                            'value' => $name
                        ];
                    }
                    return null;
                })->filter()->values(),
            'pageTemplates' => $pageTemplates,
            'app' => [
                'env' => config('app.env'),
                'title' => config('app.title'),
                'lang' => App::getLocale()
            ],
            'lang' => collect(__('pk::admin'))->merge([

            ])->toArray(),
            'langs' => Langs::get()
        ]);
    }

    public function home()
    {
        return response()->json([]);
    }

    protected function menu()
    {
        return Menu::set([
            [
                'title' => 'instance.pages',
                'url' => '/pages',
                'icon' => 'newspaper',
                'sort' => 10
            ],
            [
                'title' => 'instance.posts',
                'url' => '/posts',
                'icon' => 'file-alt',
                'sort' => 20
            ],
            [
                'title' => 'instance.categories',
                'url' => '/categories',
                'icon' => 'cabinet-filing',
                'sort' => 30
            ],
            [
                'title' => 'instance.tags',
                'url' => '/tags',
                'icon' => 'tags',
                'sort' => 40
            ],
            //                [
            //                    'title' => __('admin.routes.taxonomies'),
            //                    'url' => '/taxonomies',
            //                    'icon' => 'sitemap',
            //                    'sort' => 50
            //                ],
            [
                'title' => 'instance.images',
                'url' => '/images',
                'icon' => 'images',
                'sort' => 60
            ],
            [
                'title' => 'instance.users',
                'icon' => 'users',
                'url' => '/users',
                'sort' => 70
            ],
            [
                'title' => 'instance.roles',
                'icon' => 'user-lock',
                'url' => '/roles',
                'sort' => 80
            ],
            [
                'title' => 'instance.settings',
                'icon' => 'cog',
                'url' => '/settings',
                'sort' => 90
            ],
            [
                'title' => 'instance.account',
                'icon' => 'user',
                'url' => '/account',
                'sort' => 100
            ],
        ])->set(app()->adminMenu)->set(config('pk.adminMenu', []))->get();
    }
}
