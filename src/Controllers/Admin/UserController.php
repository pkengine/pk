<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;
use PK\Resources\Admin\UserResource;
use PK\Models\User;
use Illuminate\Http\Request;
use PK\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public $namespace = 'Admin';
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);

        $query = User::search()->orderBy('id', 'DESC');
        return $this->returnPaginate($query, 'User');
    }

    public function edit(User $user)
    {
        return response()->json(new UserResource($user));
    }

    public function list(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'in' => 'array',
            'in.*' => 'integer'
        ]);
        $tags = User::search()->limit(10)->get();
        return response()->json(UserResource::collection($tags));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'first_name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'email' => 'required|email',
            'lang' => ['nullable', Rule::in(config('app.langs'))],
            'password' => 'required|string|min:6|max:40|confirmed'
        ]);
        if(!Arr::get($requestData, 'lang')){
            $requestData['lang'] = App::getLocale();
        }
        if(isset($requestData['password']) && $requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }
        $user = User::create($requestData);
        return response()->json(new UserResource($user));
    }

    public function restore(Request $request, $user)
    {
        $user = User::onlyTrashed()->findOrFail($user);
        $user->restore();
        return response()->json(new UserResource($user));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PK\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $requestData = $request->validate([
            'first_name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'email' => 'required|email',
            'lang' => ['nullable', Rule::in(config('app.langs'))],
            'password' => 'nullable|string|min:6|max:40|confirmed',
        ]);
        if(!Arr::get($requestData, 'lang')){
            $requestData['lang'] = App::getLocale();
        }
        if(isset($requestData['password']) && $requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }
        $user->update($requestData);
        return response()->json(new UserResource($user));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $user = User::withTrashed()->findOrFail($user);

        if ($user->trashed()) {
            $user->forceDelete();
            return response()->json(['success' => 1]);
        } else {
            $user->delete();
            return response()->json(new UserResource($user));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PK\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function account(Request $request)
    {
        $requestData = $request->validate([
            'first_name' => 'required|string|max:40',
            'last_name' => 'required|string|max:40',
            'email' => 'required|email',
            'lang' => ['required', Rule::in(config('app.langs'))],
            'password' => 'nullable|string|min:6|max:40|confirmed',
        ]);
        if(isset($requestData['password']) && $requestData['password']){
            $requestData['password'] = bcrypt($requestData['password']);
        }else{
            unset($requestData['password']);
        }
        $user = Auth::user();
        $user->update($requestData);
        return response()->json(new UserResource($user));
    }
}
