<?php


namespace PK\Controllers\Admin;


use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use PK\Controllers\Controller;
use PK\Models\Post;

class InstanceController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);
        $query = \PK\Models\Lang::query()
            ->where('lang', App::getLocale())
            ->where('title', 'like', '%Мен%')
            ->with('langable');

        return $this->returnPaginate($query, 'LangInstance');
    }
}
