<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Arr;
use PK\Resources\Admin\CategoryResource;
use PK\Models\MetaField;
use PK\Models\Category;
use Illuminate\Http\Request;
use PK\Controllers\Controller;
use PK\Rules\CategoryParentRule;

class CategoryController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string',
            'parent' => 'nullable',
            'post' => 'nullable',
            'current' => 'nullable|integer'
        ]);
        $query = Category::query()->with('parent');
        if($request->s){
            $query = $query->search(['title']);
        }

        if(
            Arr::get($requestData, 'parent') &&
            Arr::get($requestData, 'current') &&
            $category = Category::find(Arr::get($requestData, 'current'))
        ){
            $ids = $category->children_recursive_ids;
            $ids[] = (int)Arr::get($requestData, 'current');
            $query = $query->whereNotIn('id', $ids);
        }

        if(Arr::get($requestData, 'post')){
            $query = $query->with('children');
        }

        return $this->returnPaginate($query, 'Category');
    }

    public function tree()
    {
        $categories = Category::query()->where('parent_id', null)->sorted()->with('childrenRecursive')->get();
        return response()->json(CategoryResource::collection($categories->sortBy('sort')));
    }

    public function make()
    {
        return response()->json(new CategoryResource(Category::make()->load(Category::FULL_RELATIONS)));
    }

    public function edit(Request $request, Category $category)
    {
        $category->load(Category::FULL_RELATIONS);
        return response()->json(new CategoryResource($category));
    }

    public function show(Request $request, Category $category)
    {
        $category->load(Category::FULL_RELATIONS);
        return response()->json(new CategoryResource($category));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'parent_id' => 'nullable|exists:categories,id',
            'settings' => 'array'
        ], Category::getValidate()));
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $category = Category::create($requestData);
        $category->saveLangs($requestData['langs']);
        $category->saveSettings(Arr::get($requestData,'settings', []));
        $category->refresh();
        $category->load(Category::FULL_RELATIONS);
        return response()->json(new CategoryResource($category));
    }


    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Category $category)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'parent_id' => ['nullable','exists:categories,id', new CategoryParentRule($category)],
            'settings' => 'array'
        ], Category::getValidate()));
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $category->update($requestData);
        $category->saveLangs($requestData['langs']);
        $category->saveSettings(Arr::get($requestData,'settings', []));
        $category->refresh();
        $category->load(Category::FULL_RELATIONS);
        return response()->json(new CategoryResource($category));
    }

    public function sort(Category $category, $type)
    {
        $type = $type === 'down' ? 'down' : 'up';
        $sisterQuery = Category::query();
        if($type === 'down'){
            $sisterQuery = $sisterQuery->orderBy('sort')
                ->where('sort', '>', $category->sort);
        }else{
            $sisterQuery = $sisterQuery->orderByDesc('sort')
                ->where('sort', '<', $category->sort);
        }
        if($category->parent_id){
            $sisterQuery = $sisterQuery->where('parent_id', $category->parent_id);
        }
        if($sister = $sisterQuery->first()){
            $newSort = $sister->sort;
            $sister->update(['sort' => $category->sort]);
            $category->update(['sort' => $newSort]);
        }
        return $this->tree();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        $category = Category::withTrashed()->findOrFail($category);
        $category->langs()->delete();
        $category->settings()->delete();
        $category->delete();
        return response()->json(['success' => 1]);
    }

}
