<?php

namespace PK\Controllers\Admin;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use PK\Resources\Admin\TaxonomyResource;
use PK\Models\MetaField;
use PK\Models\Taxonomy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PK\Controllers\Controller;

class TaxonomyController extends Controller
{
    public $namespace = 'Admin';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);
        $query = Taxonomy::query();
        if($request->s){
            $query = $query->search(['title']);
        }

        return $this->returnPaginate($query, 'Taxonomy');
    }

    public function make()
    {
        return response()->json(new TaxonomyResource(Taxonomy::make()->load(Taxonomy::FULL_RELATIONS)));
    }

    public function edit(Request $request, Taxonomy $taxonomy)
    {
        $taxonomy->load(Taxonomy::FULL_RELATIONS);
        return response()->json(new TaxonomyResource($taxonomy));
    }

    public function show(Request $request, Taxonomy $taxonomy)
    {
        $taxonomy->load(Taxonomy::FULL_RELATIONS);
        return response()->json(new TaxonomyResource($taxonomy));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
        ]);
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $taxonomy = Taxonomy::create($requestData);
        $taxonomy->saveLangs($requestData['langs']);
        $taxonomy->refresh();
        $taxonomy->load(Taxonomy::FULL_RELATIONS);
        return response()->json(new TaxonomyResource($taxonomy));
    }


    /**
     * @param Request $request
     * @param Taxonomy $taxonomy
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Taxonomy $taxonomy)
    {
        $requestData = $request->validate([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
        ]);
        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $taxonomy->update($requestData);
        $taxonomy->saveLangs($requestData['langs']);
        $taxonomy->refresh();
        $taxonomy->load(Taxonomy::FULL_RELATIONS);
        return response()->json(new TaxonomyResource($taxonomy));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $taxonomy
     * @return \Illuminate\Http\Response
     */
    public function destroy($taxonomy)
    {
        $taxonomy = Taxonomy::withTrashed()->findOrFail($taxonomy);
        $taxonomy->langs()->delete();
        $taxonomy->delete();
        return response()->json(['success' => 1]);
    }

}
