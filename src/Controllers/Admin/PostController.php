<?php

namespace PK\Controllers\Admin;

use phpDocumentor\Reflection\Types\Integer;
use PK\Resources\Admin\Collection;
use PK\Resources\Admin\PostResource;
use PK\Models\MetaField;
use PK\Models\Post;
use PK\Models\PostHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use PK\Controllers\Controller;
use Illuminate\Validation\Rule;

class PostController extends Controller
{
    public $namespace = 'Admin';

    public $type = 'post';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $requestData = $request->validate([
            'category_id' => 'nullable|string',
            'is_deleted' => 'nullable|boolean',
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string',
            'type' => 'string'
        ]);
        $query = Post::query()->with('category', 'settings')->type($request->get('type', $this->type))->withTrashed()->orderByDesc('id')->search(['title']);

        if($request->is_deleted){
            $query = $query->onlyTrashed();
        }
        if(Arr::get($requestData, 'category_id')){
            $query = $query->where('category_id', Arr::get($requestData, 'category_id'));
        }
        return $this->returnPaginate($query, 'Post');
    }


    public function list(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'include' => 'array',
            'include.*' => 'integer',
            'type' => 'string'
        ]);

        $posts = Post::where('title', 'LIKE', '%'.Arr::get($requestData, 's', '').'%')
            ->type($request->get('type', $this->type))
            ->limit(50)->get();
        if($include = $request->get('include')){
            $includePosts = Post::whereIn('id', $include)->limit(64)->get();
            $posts = $posts->merge($includePosts);
        }
        return response()->json(PostResource::collection($posts));
    }

    public function make()
    {
        return response()->json(new PostResource(Post::make()->load(Post::FULL_RELATIONS)));
    }

    public function edit(Request $request, Post $post)
    {
        $post->load(Post::FULL_RELATIONS);
        return response()->json(new PostResource($post));
    }

    public function show(Request $request, Post $post)
    {
        $post->load(Post::FULL_RELATIONS);
        return response()->json(new PostResource($post));
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'is_publish' => 'nullable|boolean',
            'category_id' => 'nullable|exists:categories,id',
            'category_ids' => 'nullable|array',
            'category_ids.*' => 'nullable|exists:categories,id',
            'tags_ids' => 'nullable|array',
            'tags_ids.*' => 'integer|exists:tags,id',
            'settings' => 'array'
        ], Post::getValidate(['type' => $this->type])));

        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $post = Post::create($requestData);
        $post->tags()->sync((array)Arr::get($requestData,'tags_ids', []));
        $post->saveLangs($requestData['langs']);
        $post->saveCategories(Arr::get($requestData, 'category_ids', []));
        $post->saveSettings(Arr::get($requestData,'settings', []));
        $post->refresh();
        $post->load(Post::FULL_RELATIONS);
        return response()->json(new PostResource($post));
    }


    /**
     * @param Request $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, Post $post)
    {
        //MemberPermissions::checkMy('posts.update', $post->author_id);
        $requestData = $request->validate(array_merge([
            'slug' => 'nullable|string|max:64',
            'langs' => 'required|array',
            'langs.*.title' => 'nullable|string|max:64',
            'langs.*.description' => 'nullable|string|max:1000',
            'langs.*.content' => 'nullable|string|max:64768',
            'image_id' => 'nullable|exists:images,id',
            'is_publish' => 'nullable|boolean',
            'category_id' => 'nullable|exists:categories,id',
            'category_ids' => 'nullable|array',
            'category_ids.*' => 'nullable|exists:categories,id',
            'tags_ids' => 'nullable|array',
            'tags_ids.*' => 'integer|exists:tags,id',
            'settings' => 'array'
        ], Post::getValidate(['type' => $this->type])));

        $requestData['slug'] = (string)Arr::get($requestData, 'slug');
        $post->update($requestData);
        $post->tags()->sync((array)Arr::get($requestData,'tags_ids', []));
        $post->saveLangs($requestData['langs']);
        $post->saveCategories(Arr::get($requestData, 'category_ids', []));
        $post->saveSettings(Arr::get($requestData,'settings', []));
        $post->refresh();
        $post->load(Post::FULL_RELATIONS);
        return response()->json(new PostResource($post));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Http\Response
     */
    public function restoreTrash(Post $post)
    {
        if($post->trashed()){
            $post->restore();
        }

        $post->load(Post::FULL_RELATIONS);
        return response()->json(new PostResource($post));
    }

    /**
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->trashed()) {
            $post->langs()->delete();
            $post->settings()->delete();
            $post->forceDelete();
        } else {
            $post->delete();
            return response()->json(new PostResource($post));
        }
        return response()->json(['success' => 1]);
    }
}
