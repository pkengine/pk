<?php

namespace PK\Controllers\Admin;

use PK\Controllers\Controller;
use PK\Resources\Admin\ImageResource;
use PK\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageIntervention;

class ImageController extends Controller
{
    public $namespace = 'Admin';

    public function index(Request $request)
    {
        $requestData = $request->validate([
            's' => 'nullable|string',
            'page' => 'nullable|integer',
            'desc' => 'nullable|string'
        ]);

        $query = Image::search(['alt', 'path'])->orderByDesc('id');
        return $this->returnPaginate($query, 'Image');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|image|max:20000'
        ]);

        $image = Image::upload(
            $request->file,
            $request->file->getClientOriginalName(),
            $request->file->getClientOriginalExtension()
        );

        return response()->json(new ImageResource($image));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \PK\Models\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        $requestData = $request->validate([
            'alt' => 'required|string|max:300',
        ]);
        $image->update($requestData);
        return response()->json(new ImageResource($image));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Image $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();
        return response()->json(['success' => 1]);
    }
}
