<?php

namespace PK\Controllers\Admin;

use Illuminate\Validation\Rule;
use PK\Facades\Settings;
use PK\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class SettingController extends Controller
{
    public function index()
    {
        return response()->json(Settings::all());
    }

    public function edit(Request $request)
    {
        $request->validate([
            'lang' => ['required', Rule::in(config('app.langs', ['ru']))]
        ]);
        return response()->json(Settings::all($request->lang));
    }

    public function update(Request $request)
    {
        $request->validate([
            'lang' => ['required', Rule::in(config('app.langs', ['ru']))]
        ]);
        $requestData = $request->validate(Settings::provider()->validate());

        $settings = Settings::provider()->getSettings();
        foreach($settings as $setting => $type){
            Settings::set($setting, Arr::get($requestData, $setting), $type, $request->lang);
        }
        Settings::refresh();
        return response()->json(Settings::all($request->lang));
    }
}
