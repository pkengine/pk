<?php


namespace PK\Controllers\App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use PK\Controllers\Controller;

class LangController extends Controller
{
    public function set(Request $request, $lang)
    {
        if(in_array($lang, config('app.langs', ['ru']))){
            Session::put('lang', $lang);
        }
    }
}
