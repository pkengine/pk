<?php


namespace PK\Controllers\App;

use Illuminate\Http\Request;
use PK\Controllers\Controller;
use PK\Services\SitemapManager;

class SitemapController extends Controller
{
    public function get(Request $request, $instance = null)
    {
        $xml = new SitemapManager();
        if($instance){
            $xml->generate($instance);
        }else{
            $xml->generateMap();
        }

        return response($xml->getXML(), 200, [
            'Content-Type' => 'application/xml'
        ]);
    }
}
