<?php


namespace PK\Controllers\App;


use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;
use PK\Controllers\Controller;
use PK\Mail\OrderCreated;
use PK\Models\Cart;
use PK\Models\Order;
use PK\Resources\App\CartResource;
use PK\Resources\App\OrderResource;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $requestData = $request->validate([
            'phone' => 'required|string|min:7|max:20',
            'email' => 'required|email',
            'address' => 'required|string|min:10|max:300',
            'delivery_datetime' => 'required|date',
        ]);
        $cart = Cart::current()->refresh();
        if($cart->isEmpty()){
            throw ValidationException::withMessages([
                'cart' => [trans('cart.failed')],
            ]);
        }

        $requestData['delivery_datetime'] = Carbon::parse($request->get('delivery_datetime'));
        $order = Order::makeFromCart($cart);
        $order->fill($requestData);
        $order->save();
        event('order:created', $order);
        return response()->json([
            'success' => 1,
            'cart' => new CartResource($cart->flush()),
            'order' => new OrderResource($order)
        ]);
    }
}
