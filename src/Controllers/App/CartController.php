<?php


namespace PK\Controllers\App;


use Illuminate\Http\Request;
use PK\Controllers\Controller;
use PK\Models\Cart;
use PK\Models\Post;
use PK\Resources\App\CartResource;

class CartController extends Controller
{
    public $namespace = 'App';

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return response()->json(new CartResource(Cart::current()->refresh()));
    }

    public function addItem(Request $request, Post $product)
    {
        $request->validate([
            'count' => 'nullable|integer|min:1'
        ]);
        $cart = Cart::current();
        $cart->addItem($product->id, max((int)$request->get('count', 1), 1));
        return response()->json(new CartResource($cart->refresh()));
    }

    public function subItem(Request $request, Post $product)
    {
        $request->validate([
            'count' => 'nullable|integer|min:1'
        ]);
        $cart = Cart::current();
        $cart->subItem($product->id, max((int)$request->get('count', 1), 1));
        return response()->json(new CartResource($cart->refresh()));
    }

    public function removeItem(Request $request, Post $product)
    {
        $cart = Cart::current();
        $cart->subItem($product->id);
        return response()->json(new CartResource($cart->refresh()));
    }
}
