<?php


namespace PK\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use PK\Listeners\OrderSubscriber;

class EventServiceProvider extends ServiceProvider
{

    protected $subscribe = [
        OrderSubscriber::class,
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
