<?php

namespace PK\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Storage;
use PK\Commands\UpdateCommand;
use PK\Facades\Settings;
use PK\Services\MenuManager;
use PK\Middleware\Install;
use PK\Services\LangManager;
use PK\Services\SettingManager;
use Illuminate\Support\ServiceProvider;

class PKServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->adminMenu = [];
        $this->app->bind('settings',function() {
            return new SettingManager();
        });
        $this->app->bind('langs',function() {
            return new LangManager();
        });
        $this->app->bind('menu',function() {
            return new MenuManager();
        });
        $this->app['config']['filesystems.disks.pk'] = [
            'driver' => 'local',
            'root' => __DIR__.'/../../',
        ];
        $this->app['config']['filesystems.disks.pkengine'] = [
            'driver' => 'local',
            'root' => __DIR__.'/../../../',
        ];
        $this->app['translator']->addNamespace('pk', Storage::disk('pk')->path('resources/lang'));

        AliasLoader::getInstance(['Front' => \PK\Helpers\FrontHelper::class]);
        AliasLoader::getInstance(['Image' => \Intervention\Image\Facades\Image::class]);
        AliasLoader::getInstance(['Settings' => Settings::class]);
        $this->loadRoutesFrom(__DIR__.'/../../routes/admin.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/admin');
        $this->loadViewsFrom(__DIR__.'/../../../../public', 'public');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/admin', 'admin');
        $this->loadViewsFrom(__DIR__.'/../../resources/views/email', 'admin.email');
        $this->publishes([
            __DIR__.'/../../config/' => config_path(),
        ], 'config');
        $this->publishes([
            __DIR__.'/../../resources/lang/' => resource_path('lang'),
        ], 'lang');

        Blade::directive('hasSetting', function ($expression) {
            return "<?php if('\PK\Facades\Settings::get('{$expression}')) { ?>";
        });
        Blade::directive('setting', function ($expression) {
            return "<?php echo \PK\Facades\Settings::get('{$expression}'); ?>";
        });

        $permissions = require __DIR__.'/../../config/permissions.php';
        $this->app->permissions = collect($permissions ?? [])->merge(config('permissions', []))->toArray();

        $this->app->register(ShopServiceProvider::class);
        $this->app->register(EventServiceProvider::class);

        if ($this->app->runningInConsole()) {
            $this->commands([
                UpdateCommand::class
            ]);
        }
    }
}
