<?php


namespace PK\Providers;


use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(in_array('shop', config('pk.plugins', []))){
            Config::set('plugins.shop', true);
            $this->loadMigrationsFrom(__DIR__.'/../../database/shop');
            $this->loadRoutesFrom(__DIR__.'/../../routes/shop.php');
            $this->setMenu();
        }
    }

    private function setMenu()
    {
        $this->app->adminMenu = array_merge($this->app->adminMenu, [
            [
                'title' => 'instance.products',
                'url' => '/products',
                'icon' => 'shopping-basket',
                'sort' => 25
            ],
            [
                'title' => 'instance.orders',
                'url' => '/orders',
                'icon' => 'box',
                'sort' => 28
            ],
        ]);
    }
}
