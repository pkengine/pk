<?php


namespace PK\Providers;


use Illuminate\Support\Str;
use PK\Traits\SettingsShopTrait;

class SettingsProvider
{
    use SettingsShopTrait;

    public $excludeLang = [];

    public $relations = [];

    public function validate()
    {
        return [];
    }

    public function getSettings()
    {
        return [];
    }

    public function __call($method, $args)
    {
        if(is_array($return = $this->getCall($method))){
            return $return;
        }
    }

    public function __get($var)
    {
        if(is_array($return = $this->getCall($var))){
            return $return;
        }
    }

    private function getCall($var)
    {
        if(
            Str::startsWith($var, 'get') && Str::endsWith($var, 'Settings') ||
            Str::startsWith($var, 'exclude') && Str::endsWith($var, 'Lang') ||
            Str::startsWith($var, 'validate') && $var !== 'validate'
        ){
            $result = [];
            if(method_exists($this, $var)){
                $result = array_merge($result, $this->$var());
            }
            if(property_exists($this, $var)){
                $result = array_merge($result, $this->$var);
            }
            return $result;
        }
    }
}
