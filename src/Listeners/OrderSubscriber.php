<?php


namespace PK\Listeners;


use Illuminate\Support\Facades\Mail;
use PK\Facades\Settings;
use PK\Mail\OrderCreated;
use PK\Models\Order;

class OrderSubscriber
{

    public function eventCreated(Order $order)
    {

        if($email = Settings::get('sendemail')){
            try{
                Mail::to($email)
                    ->send(new OrderCreated($order));
                Mail::to($order->email)
                    ->send(new OrderCreated($order));
            }catch (\Exception $e){

            }
        }

    }

    public function subscribe($events)
    {
        $events->listen(
            'order:created',
            [static::class, 'eventCreated']
        );
    }
}
