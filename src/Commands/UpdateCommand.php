<?php


namespace PK\Commands;


use Illuminate\Console\Command;

class UpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pk:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update site';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $path = 'cd '. base_path().'; ';
//        exec($path.'php composer.phar update');
        return 0;
    }
}
