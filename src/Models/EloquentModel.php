<?php


namespace PK\Models;


use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PK\Traits\SearchTrait;
use PK\Traits\TimezoneAtTrait;

class EloquentModel extends Model
{
    use TimezoneAtTrait, SearchTrait;

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        $query = $this->query();

        if($this instanceof Post){

            $plural = Str::between(app('request')->route()->getName(), '.', '.');
            $one = $this->pluralType($plural);

            $query = $query->type($one);
        }

        if(Auth::user() && method_exists($this, 'bootSoftDeletes')){
            $query = $query->withTrashed();
        }
        return $query->where('id', (int)$value)->firstOrFail();
    }

    public static function factory(...$parameters)
    {
        return Factory::factoryForModel(class_basename(get_called_class()))
            ->count(is_numeric($parameters[0] ?? null) ? $parameters[0] : null)
            ->state(is_array($parameters[0] ?? null) ? $parameters[0] : ($parameters[1] ?? []));
    }
}
