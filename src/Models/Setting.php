<?php

namespace PK\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;

class Setting extends EloquentModel
{
    protected $fillable = [
        'name', 'value', 'type', 'lang'
    ];

    protected static function booted()
    {
        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->where(function ($query){
                $query->whereNull('lang')->orWhere('lang', App::getLocale());
            });
        });
    }

    public function getValueSettingAttribute()
    {
        $value = $this->getOriginal('value');

        if($this->type == 'boolean'){
            return is_bool($value) ? $value : json_decode($value, true);
        }elseif($this->type == 'array'){
            return is_array($value) ? $value : json_decode($value, true);
        }elseif($this->type == 'object'){
            return json_decode($value);
        }elseif ($this->type == 'relation'){
            return json_decode($value, true);
        }else{
            return $value;
        }
    }

    public function getValueAttribute($value)
    {
        return !is_array($value) ? json_decode($value, true) : $value;
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = !is_string($value) ? json_encode($value) : $value;
    }

    public function settingable()
    {
        return $this->morphToMany();
    }
}
