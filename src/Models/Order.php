<?php


namespace PK\Models;


use Illuminate\Support\Arr;

class Order extends EloquentModel
{
    const STATUSES = [
        0 => 'created',
        1 => 'processing',
        2 => 'processed',
        3 => 'canceled',
        4 => 'delivery',
        5 => 'complited',
        6 => 'returning',
        7 => 'returned',
    ];

    protected $casts = [
        'items' => 'json',
    ];

    protected $dates = [
        'delivery_datetime',
    ];

    protected $fillable = [
        'user_id', 'phone', 'email', 'address', 'status', 'history', 'items', 'price',
        'delivery_price', 'delivery_datetime'
    ];

    public static function makeFromCart(Cart $cart)
    {
        $order = new static();
        $items = collect($cart->products)->map(function ($item){
            return [
                'id' => $item->id,
                'title' => $item->lang->title,
                'price' => $item->price,
                'count' => $item->count,
                'total_price' => $item->total_price,
                'image_url_thumb' => $item->image ? $item->image->url_thumb : null,
                'image_url_thumb_p' => $item->image ? $item->image->url_thumb_p : null,
            ];
        });
        $order->fill([
            'items' => $items,
            'price' => $cart->total_price,
            'status' => 0
        ]);
        return $order;
    }

    public function getStatusTitleAttribute()
    {
        $key = Arr::get(static::STATUSES, (string)$this->status);
        return __('pk::admin.order.status.'.$key);
    }
}
