<?php

namespace PK\Models;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageIntervention;
use PK\Traits\SearchTrait;

class Image extends EloquentModel
{
    use SearchTrait;

    protected $fillable = [
        'path', 'path_thumb', 'path_thumb_p', 'alt', 'size', 'width', 'height', 'width_thumb', 'height_thumb', 'hash'
    ];

    public function getSize($width, $height, $crop = true)
    {
        if(!Storage::disk('public')->exists($this->path)) return null;
        $hash = md5($width.'-'.$height.'-'.(int)$crop);
        $ext = Str::afterLast($this->path, '.');
        $path = Str::beforeLast($this->path, '.').'-'.$hash.'.'.$ext;

        if(!Storage::disk('public')->exists($path)){
            $img = ImageIntervention::make(Storage::disk('public')->get($this->path));
            if($width || $height){
                if($crop){
                    $img = $img->fit($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                }else{
                    $img = $img->resize($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

            }

            $img->save(Storage::disk('public')->path($path), 60);

        }
        return Storage::disk('public')->url($path);
    }

    /**
     * @param $file
     * @param string $originName
     * @param string $ext
     * @param string|null $date
     */
    public static function upload($file, string $originName, string $ext, $date = null)
    {
        $image = ImageIntervention::make($file);
        $date = $date ?: date('Y-m');
        $pathFolder = '/files/images/'.$date.'/';
        $name = (string)Str::uuid();
        $ext = '.'.$ext;
        $hash = sha1($image->encode()->encoded);
        if($hash && ($oldImage = Image::where('hash', $hash)->first())){
            return $oldImage;
        }
        $imageOriginal = clone($image)->resize(1920, 1920, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $imageThumb = clone($image)->resize(540, 360, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $imageThumbP = clone($image)->fit(240, 240, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $original = $pathFolder.$name.$ext;
        $thumb = $pathFolder.$name.'_t'.$ext;
        $thumb_p = $pathFolder.$name.'_t_p'.$ext;
        Storage::disk('public')->put($original, $imageOriginal->stream());
        Storage::disk('public')->put($thumb, $imageThumb->stream());
        Storage::disk('public')->put($thumb_p, $imageThumbP->stream());
        return Image::create([
            'path' => $original,
            'path_thumb' => $thumb,
            'path_thumb_p' => $thumb_p,
            'alt' => Str::beforeLast($originName, '.'),
            'size' => filesize(Storage::disk('public')->path($original)),
            'width' => $imageOriginal->width(),
            'height' => $imageOriginal->height(),
            'width_thumb' => $imageThumb->width(),
            'height_thumb' => $imageThumb->height(),
            'hash' => $hash
        ]);
    }

    public function getUrlAttribute()
    {
        return Storage::disk('public')->url($this->path);
    }

    public function getUrlThumbAttribute()
    {
        return Storage::disk('public')->url($this->path_thumb);
    }

    public function getUrlThumbPAttribute()
    {
        return Storage::disk('public')->url($this->path_thumb_p);
    }
}
