<?php


namespace PK\Models;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Cart extends EloquentModel
{
    protected $casts = [
        'items' => 'json'
    ];

    protected $fillable = [
        'user_id', 'items', 'session'
    ];

    /**
     * @return Cart
     */
    public static function current()
    {
        if(Auth::check()){
            return Cart::firstOrCreate(['user_id' => Auth::id()], ['items' => []]);
        }elseif($session = Session::get('shop_session')){
            return Cart::firstOrCreate(['session' => $session], ['items' => []]);
        }
        $session = sha1(Str::random());
        Session::put('shop_session', $session);
        return Cart::create(array_merge(['session' => $session], ['items' => []]));
    }

    /**
     * @return $this|Cart
     * @throws \Exception
     */
    public function flush()
    {
        $this->delete();
        return static::current()->refresh();
    }

    /**
     * @param int $productId
     * @param int $count
     */
    public function addItem(int $productId, $count = 1)
    {
        $items = $this->items;
        $cartCount = Arr::get($items, $productId, 0);
        $cartCount = $cartCount + $count;
        Arr::set($items, $productId, $cartCount);

        $this->update(['items' => $items]);
    }

    /**
     * @param int $productId
     * @param int $count
     */
    public function subItem(int $productId, $count = 1)
    {
        $items = $this->items;
        $cartCount = Arr::get($items, $productId, 0);
        $cartCount = $cartCount - $count;
        if($cartCount > 0){
            Arr::set($items, $productId, $cartCount);
        }else{
            unset($items[$productId]);
        }
        $this->update(['items' => $items]);
    }

    /**
     * @param int $productId
     */
    public function removeItem(int $productId)
    {
        $items = $this->items;
        unset($items[$productId]);
        $this->update(['items' => $items]);
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return count($this->items) < 1;
    }

    /**
     * @return $this|Cart
     */
    public function refresh()
    {
        $products = Post::type('product')->whereIn('id', array_keys($this->items))->get()->map(function ($item){
            $item->setAttribute('count', Arr::get($this->items, $item->id));
            $item->setAttribute('price', $item->getSetting('price'));
            $item->setAttribute('total_price', $item->getSetting('price') * Arr::get($this->items, $item->id));
            return $item;
        });
        $ids = $products->pluck('id');
        $this->items = collect($this->items)->only($ids)->toArray();
        $this->setAttribute('discount', 0);
        $this->setAttribute('products', $products);
        $this->setAttribute('count', $products->count());
        $this->setAttribute('products_price', $products->sum('total_price'));
        $this->setAttribute('delivery_price', 0);
        event('cart:refresh', $this);
        $this->setAttribute('total_price', $this->getAttribute('products_price') - $this->getAttribute('discount') + $this->getAttribute('delivery_price'));
        $this->update();
        return $this;
    }

    /**
     * @param array $attributes
     * @param array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $this->fill($attributes);
        $instanceAttributes = $this->attributes;
        $this->attributes = Arr::only($this->attributes, $this->fillable);
        $return = parent::update($attributes, $options);
        $this->attributes = $instanceAttributes;
        return $return;
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = [])
    {
        $instanceAttributes = $this->attributes;
        $this->attributes = Arr::only($this->attributes, $this->fillable);
        $return = parent::save($options);
        $this->attributes = $instanceAttributes;
        return $return;
    }
}
