<?php

namespace PK\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use PK\Traits\ImageTrait;
use PK\Traits\SettingTrait;
use PK\Traits\SlugTrait;
use PK\Traits\LangTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageIntervention;

class Post extends EloquentModel
{
    use SoftDeletes, SlugTrait, LangTrait, SettingTrait, ImageTrait;

    const FULL_RELATIONS = ['category', 'tags', 'settings', 'categories'];

    protected $fillable = [
        'slug', 'category_id', 'is_publish', 'image_id', 'type'
    ];

    protected $types = ['post', 'product'];

    protected $pluralTypes = [
        'posts' => 'post',
        'products' => 'product'
    ];

    protected static function booted()
    {
        if(Auth::guest()){
            static::addGlobalScope('publish', function (Builder $builder) {
                $builder->where('is_publish', true)->orderBy('created_at', 'DESC');
            });
        }
        if(Auth::check()){
            static::addGlobalScope('visible', function (Builder $builder) {
                $builder->withTrashed();
            });
        }

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'DESC');
        });

        static::addGlobalScope('image', function (Builder $builder) {
            $builder->with('image');
        });

        static::addGlobalScope('lang', function (Builder $builder) {
            $builder->with('lang', 'langs');
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class)->withPivot(['step']);
    }

    public function saveCategories($categories)
    {
        $parent = $this->category_id;
        $this->categories()->sync([]);
        foreach($categories as $step => $categoryId){
            if(Category::where('parent_id', $parent)->find($categoryId)){
                $parent = $categoryId;
                $this->categories()->attach($categoryId, ['step' => $step]);
            }else{
                break;
            }
        }
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function scopeCategory($query, $slug)
    {
        $query->whereHas('category', function ($query) use ($slug){
            $query->where('slug', $slug);
        });
    }

    public function scopeWithSlug($query)
    {
        $query->whereNotNull('slug')->where('slug', '!=', '');
    }

    public function scopeType($query, $type = 'post')
    {
        $query->where('type', $type ? $type : 'post');
    }

    public function scopeAdminWithTrashed($query)
    {
        if(Auth::check()){
            $query->withTrashed();
        }
    }

    public function pluralType($plural)
    {
        return Arr::get($this->pluralTypes, $plural, $plural);
    }
}
