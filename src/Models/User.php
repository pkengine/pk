<?php

namespace PK\Models;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use PK\Traits\SearchTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use PK\Traits\SettingTrait;
use PK\Traits\TimezoneAtTrait;

class User extends Authenticatable
{
    use TimezoneAtTrait, SearchTrait, Notifiable, SettingTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
        'user_type', 'lang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setSettings()
    {
        $this->setLocale();
        $this->setTimezone();
    }

    public function setLocale()
    {
        $locale = $this->lang ?? config('app.locale', 'ru');
        Config::set('app.locale', $locale);
        Config::set('app.fallback_locale', $locale);
        App::setLocale($locale);
        $locale = $locale . '_' . mb_strtoupper($locale) . '.UTF-8';//'ru_RU.UTF-8'
        setlocale(LC_ALL, $locale);
    }

    public function setTimezone()
    {
        if ($timezoneName = timezone_name_from_abbr("", (int)$this->timezone * 3600, false)) {
            Config::set('app.timezone', $timezoneName);
            Config::set('app.timezone_utc', (int)$this->timezone);
//            //Config::set('database.connections.'.config('database.default').'.timezone', $timezoneName);
//            Config::set('app.timezone_is_set', true);
            //DB::purge(config('database.default'));
//            date_default_timezone_set($timezoneName);
        }
    }
}
