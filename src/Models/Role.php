<?php


namespace PK\Models;


use PK\Traits\LangTrait;

class Role extends EloquentModel
{
    use LangTrait;

    protected $casts = [
        'permissions' => 'array'
    ];

    protected $fillable = [
        'permissions'
    ];
}
