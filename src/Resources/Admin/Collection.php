<?php


namespace PK\Resources\Admin;

class Collection
{
    public static function pagination($pagination, $class)
    {
        $classResource = '\PK\Resources\Admin\\'.$class.'Resource';
        if(!class_exists($classResource)){
            $classResource = '\PK\Resources\\'.$class.'Resource';
        }
        return [
            'data' => $classResource::collection($pagination->getCollection()),
            'current_page' => $pagination->currentPage(),
            'per_page' => (int)$pagination->perPage(),
            'pages' => $pagination->lastPage(),
            'total' => $pagination->total(),
        ];
    }

}
