<?php


namespace PK\Resources\Admin;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;
use PK\Models\Lang;

class LangResource extends JsonResource
{
    public function toArray($request)
    {
        $this->resource = $this->resource ?: Lang::makeLang();
        return [
            'title' => (string)$this->title,
            'description' => (string)$this->description,
            'content' => (string)$this->content,
            'lang' => $this->lang
        ];

    }
}
