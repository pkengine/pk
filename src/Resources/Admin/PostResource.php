<?php

namespace PK\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'image_id' => $this->image_id,
            'image' => new ImageResource($this->whenLoaded('image')),
            'image_url' => $this->whenLoaded('image', function (){
                return $this->image ? url($this->image->path_thumb_p): null;
            }),

            'category' => new CategoryResource($this->whenLoaded('category')),
            'category_id' => $this->category_id,
            'category_ids' => $this->whenLoaded('categories', function (){
                return $this->categories->pluck('id', 'step');
            }),
            'tags' => TagResource::collection($this->whenLoaded('tags')),
            'tags_ids' => $this->whenLoaded('tags', function (){
                return $this->tags->pluck('id');
            }),

            'url' => $this->slug ? route('post', $this->slug) : null,
            'is_publish' => $this->is_publish,
            'is_deleted' => $this->trashed(),
            'created_at' => $this->atTz('created_at', '%e %b %Y, %T'),
            'updated_at' => $this->atTz('updated_at', '%e %b %Y, %T'),
            'lang' => App::getLocale(),
            'langs' => LangResource::collection($this->langs),
            $this->merge(new LangResource($this->lang)),
            'settings' => $this->getSettings(),
        ];
    }
}
