<?php

namespace PK\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class LangInstanceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $classResource = '\PK\Resources\Admin\\'.class_basename($this->langable_type).'Resource';
        return new $classResource($this->langable);
    }
}
