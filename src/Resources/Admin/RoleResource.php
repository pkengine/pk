<?php

namespace PK\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'permissions' => (array)$this->permissions,
            'lang' => App::getLocale(),
            'langs' => LangResource::collection($this->langs),
            $this->merge(new LangResource($this->lang)),
        ];
    }
}
