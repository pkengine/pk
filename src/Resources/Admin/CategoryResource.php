<?php

namespace PK\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'url' => $this->slug ? route('category', $this->slug) : null,
            'image' => new ImageResource($this->whenLoaded('image')),
            'image_url' => $this->whenLoaded('image', function (){
                return $this->image ? url($this->image->path_thumb_p): null;
            }),
            'created_at' => $this->atTz('created_at', '%e %b %Y, %T'),
            'updated_at' => $this->atTz('updated_at', '%e %b %Y, %T'),
            'lang' => App::getLocale(),
            'langs' => LangResource::collection($this->langs),
            $this->merge(new LangResource($this->lang)),
            'settings' => $this->getSettings(),
            'sort' => $this->sort,
            'parent_id' => $this->parent_id,
            'parent' => new CategoryResource($this->whenLoaded('parent')),
            $this->mergeWhen($this->whenLoaded('children'), function (){
                return ['children' => CategoryResource::collection($this->children->sortBy('sort'))];
            }),
            $this->mergeWhen($this->whenLoaded('childrenRecursive'), function (){
                return ['children' => CategoryResource::collection($this->childrenRecursive->sortBy('sort'))];
            }),
            'children_ids' => $this->whenLoaded('children', function (){
                return $this->children->pluck('id');
            }),

        ];
    }
}
