<?php


namespace PK\Resources\App;


use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'items' => collect($this->items),
            'products' => ProductResource::collection(collect($this->products)),
            'count' => $this->count,
            'total_count' => $this->total_count,
            'delivery_price' => $this->delivery_price,
            'products_price' => $this->products_price,
            'discount' => $this->discount,
            'total_price' => $this->total_price,
            'is_empty' => $this->isEmpty()
        ];
    }
}
