<?php


namespace PK\Resources\App;


use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'price' => $this->price,
            'status_title' => $this->status_title,
            'items' => $this->items,
            'address' => $this->address,
            'phone' => $this->phone,
            'email' => $this->email,
            'delivery_datetime' => $this->atTz('delivery_datetime', '%e %b %Y, %T'),
            'created_at' => $this->atTz('created_at', '%e %b %Y, %T'),
            'updated_at' => $this->atTz('updated_at', '%e %b %Y, %T'),
        ];
    }
}
