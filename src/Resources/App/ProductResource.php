<?php


namespace PK\Resources\App;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use PK\Resources\Admin\CategoryResource;
use PK\Resources\Admin\LangResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'url' => $this->slug ? route('post', $this->slug) : null,
            $this->merge(new LangResource($this->lang)),
            'settings' => $this->getSettings(),
            'image' => $this->whenLoaded('image', function (){
                return [
                    'url' => $this->image->url,
                    'url_thumb' => $this->image->url_thumb,
                    'url_thumb_p' => $this->image->url_thumb_p,
                ];
            }),
            'price' => $this->price,
            'count' => $this->count,
            'total_price' => $this->total_price
        ];
    }
}
