<?php


namespace PK\Helpers;


class FrontHelper
{
    public static function assetV($uri)
    {
        $time = @filectime(public_path($uri));
        return $uri.'?t='.$time;
    }
}
