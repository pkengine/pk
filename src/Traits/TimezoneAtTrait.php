<?php


namespace PK\Traits;

use Carbon\Carbon;


trait TimezoneAtTrait
{
    /**
     * @param string $field
     * @param null|string $format
     * @return string|null
     */
    public function atTz(string $field, $format = null)
    {
        $date = null;
        if($this->$field instanceof Carbon){
            $date = $this->$field;
        }elseif($this->$field){
            try{
                $date = Carbon::parse($this->$field);
            }catch(\Exception $e){}
        }

        if($date){
            $date->timezone((int)config('app.timezone_utc', 0));
            if($format){
                if(method_exists($date, $format)){
                    return $date->$format();
                }elseif(strripos($format, '%') === false){
                    return $date->format($format);
                }else{
                    return $date->formatLocalized($format);
                }
            }
            return $date->format('Y-m-d H:i:sO');
        }
        return $format ? '' : null;
    }
}
