<?php


namespace PK\Traits;


use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image as ImageIntervention;
use PK\Models\Image;

trait ImageTrait
{
    /**
     * @return mixed
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * @return string|null
     */
    public function getImagePathAttribute()
    {
        return $this->image ? $this->image->path : null;
    }

    /**
     * @return string|null
     */
    public function getImagePathThumbAttribute()
    {
        return $this->image ? $this->image->path_thumb : null;
    }

    /**
     * @return string|null
     */
    public function getImagePathThumbPAttribute()
    {
        return $this->image ? $this->image->path_thumb_p : null;
    }

    /**
     * @param $width
     * @param $height
     * @param bool $crop
     * @return string|null
     */
    public function getImage($width, $height, $crop = true)
    {
        if(!($image = $this->image)) return null;
        return $image->getSize($width, $height, $crop);
    }
}
