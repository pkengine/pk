<?php


namespace PK\Traits;


use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

trait SlugTrait
{
    /**
     * @param string|null $value
     * @throws ValidationException
     */
    public function setSlugAttribute($value)
    {
        if($value){
            $slug = Str::slug($value);
        }else{
            $slug = Str::slug(app('request')->get('title'), '-');
        }

        if(!$slug){
            throw ValidationException::withMessages([
                'slug' => __('slug')
            ]);
        }
        $n = 0;
        $newSlug = $slug;
        do{
            $count = static::where([
                'slug' => $newSlug
            ])->where('id', '!=', (int)$this->id)->count();
            if($count){
                ++$n;
                $newSlug = $slug.'-'.$n;
            }
        }while($count !== 0);

        $this->attributes['slug'] = $newSlug;
    }

    /**
     * @param $query
     * @param $slug
     */
    public function scopeSlug($query, $slug)
    {
        $query->where('slug', $slug);
    }
}
