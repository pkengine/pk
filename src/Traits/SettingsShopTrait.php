<?php


namespace PK\Traits;


trait SettingsShopTrait
{
    public function getPostProductSettings()
    {
        return [
            'price' => 'float',
        ];
    }

    public function validatePostProduct()
    {
        return [
            'price' => 'nullable|numeric',
        ];
    }

    public function excludePostProductLang()
    {
        return ['price'];
    }
}
