<?php


namespace PK\Traits;


use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use PK\Facades\Settings;
use PK\Models\Post;
use PK\Models\Setting;

trait SettingTrait
{
    /**
     * @return string
     */
    public function getMethodSettingsAttribute()
    {
        return 'get'.class_basename(static::class).'Settings';
    }

    /**
     * @return string
     */
    public function getMethodPostTypeSettingsAttribute()
    {
        if($this instanceof Post) {
            return 'get' . class_basename(static::class) . $this->type . 'Settings';
        }
        return $this->getMethodSettingsAttribute();
    }

    /**
     * @return string
     */
    public function getValidateSettingsAttribute()
    {
        return 'validate'.class_basename(static::class);
    }

    /**
     * @return string
     */
    public function getValidatePostTypeSettingsAttribute()
    {
        if($this instanceof Post) {
            return 'validate' . class_basename(static::class) . $this->type;
        }
        return $this->getValidateSettingsAttribute();
    }

    /**
     * @return string
     */
    public function getExcludeLangAttribute()
    {
        return 'exclude'.class_basename(static::class).'Lang';
    }

    /**
     * @return string
     */
    public function getExcludePostTypeLangAttribute()
    {
        if($this instanceof Post){
            return 'exclude'.class_basename(static::class).$this->type.'Lang';
        }
        return $this->getExcludeLangAttribute();
    }

    /**
     * @return mixed
     */
    public function settings()
    {
        return $this->morphMany(Setting::class, 'settingable');
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        if(Arr::has($this->getRelations(), 'settings')){
            $settings = collect(Settings::provider()->{$this->method_settings})
                ->merge(Settings::provider()->{$this->method_post_type_settings});

            $settingsModels = $this->settings->keyBy('name');

            $return = $this->prepareSettings(
                $settings->only(array_merge(
                    Settings::provider()->{$this->exclude_lang},
                    Settings::provider()->{$this->exclude_post_type_lang}
                )),
                $settingsModels,
                null
            );

            $langs = collect(config('app.langs', ['ru']))->mapWithKeys(function ($lang) use ($settings, $settingsModels, &$return){

                return [
                    $lang => $this->prepareSettings(
                        $settings->except(array_merge(
                            Settings::provider()->{$this->exclude_lang},
                            Settings::provider()->{$this->exclude_post_type_lang})),
                        $settingsModels,
                        $lang
                    )
                ];
            });

            $return->put('langs', $langs);
            return $return;
        }else{
            return new \stdClass();
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getSetting($name)
    {
        if($settingsModel = $this->settings->where('name', $name)->first()){
            return Settings::get($settingsModel, true, false);
        }else{
            $settings = array_merge(
                Settings::provider()->{$this->method_settings},
                Settings::provider()->{$this->method_post_type_settings}
            );
            return Settings::prepareType(Arr::get($settings, $name, 'string'));
        }
    }

    /**
     * @param Collection $settings
     * @param $settingsModels
     * @param $lang
     * @return mixed
     */
    protected function prepareSettings(Collection $settings, $settingsModels, $lang)
    {
        return $settings->map(function ($type, $name) use ($settingsModels, $lang){
            if($settingsModel = $settingsModels->filter(function ($item) use ($name, $lang){
                return $item->name === $name && $item->lang === $lang;
            })->first()){
                return Settings::get($settingsModel, true, false);
            }else{
                return Settings::prepareType($type);
            }
        });
    }

    protected static function getValidate($params = [])
    {
        $instance = new static();
        $instance->fill($params);

        $validations = array_merge(
            (array)Settings::provider()->{$instance->validate_settings},
            (array)Settings::provider()->{$instance->validate_post_type_settings},
        );

        $return = collect($validations)
            ->only(array_merge(
                Settings::provider()->{$instance->exclude_lang},
                Settings::provider()->{$instance->exclude_post_type_lang}
            ))
            ->mapWithKeys(function ($value, $key){
            return ['settings.'.$key => $value];
        })->toArray();
        $langs = collect(config('app.langs', ['ru']))->mapWithKeys(function ($item) use ($validations, $instance){
            return collect($validations)
                ->except(array_merge(
                    Settings::provider()->{$instance->exclude_lang},
                    Settings::provider()->{$instance->exclude_post_type_lang}
                ))
                ->mapWithKeys(function ($value, $key) use ($item){
                return ['settings.'.$item.'.'.$key => $value];
            });
        })->toArray();
        $return = array_merge($return, $langs);
        return $return;
    }

    public function setSetting(string $name, $value, $type = 'string', $lang = null)
    {
        if(in_array($name, array_merge(
            Settings::provider()->{$this->exclude_lang},
            Settings::provider()->{$this->exclude_post_type_lang},
        ))){
            $lang = null;
            $this->settings()->withoutGlobalScope('lang')->where(['name' => $name])->whereNotNull('lang')->delete();
        }else{
            $this->settings()->withoutGlobalScope('lang')->where(['name' => $name])->whereNull('lang')->delete();
            if(!$lang) return false;
        }
        $data = [
            'value' => json_encode($value),
            'type' => $type
        ];
        return $this->settings()->withoutGlobalScope('lang')->updateOrCreate(['name' => $name, 'lang' => $lang], $data);
    }

    public function saveSettings($settings)
    {
        foreach(Arr::get($settings, 'langs') as $lang => $items){
            collect(Settings::provider()->{$this->method_settings})
                ->merge(Settings::provider()->{$this->method_post_type_settings})
                ->except(array_merge(
                    Settings::provider()->{$this->exclude_lang},
                    Settings::provider()->{$this->exclude_post_type_lang}
                ))
                ->each(function ($type, $name) use ($lang, $items){
                    $this->setSetting($name, Arr::get($items, $name), $type, $lang);
                });
        }
        unset($settings['langs']);
        collect(Settings::provider()->{$this->method_settings})
            ->merge(Settings::provider()->{$this->method_post_type_settings})
            ->only(array_merge(
                Settings::provider()->{$this->exclude_lang},
                Settings::provider()->{$this->exclude_post_type_lang}
            ))
            ->each(function ($type, $name) use ($settings){
                $this->setSetting($name, Arr::get($settings, $name), $type, null);
            });
    }
}
