<?php

namespace PK\Traits;


use Illuminate\Support\Facades\App;

trait SearchTrait
{
    /**
     * @param $query
     * @param string $fields
     * @param null $search
     * @return Illuminate\Database\Query\Builder\Builder
     */
    public function scopeSearch($query, $fields = 'name', $search = null)
    {
        if($search = $search ?? app('request')->get('s')){
            $fields = collect($fields)->toArray();
            $query = $query->whereHas('langs', function ($query) use ($fields, $search){
                $query->where('lang', App::getLocale());
                $query->where(function ($query) use ($fields, $search) {
                    foreach($fields as $key => $field){
                        if(is_string($field)){
                            if(strpos($field, '.')){
                                list($value1, $value2) = explode('.', $field);
                                $query->orWhereHas($value1, function ($query) use ($search,$value2){
                                    $query->where($value2, 'LIKE', '%'.$search.'%');
                                });
                            } else {
                                $query->orWhere($field, 'LIKE', '%'.$search.'%');
                            }
                        }elseif(is_callable($field)){
                            $query->orWhereHas($key, $field);
                        }
                    }
                });

            });
        }
        if($in = (array)app('request')->get('in')){
            $query = $query->whereIn('id', $in);
        }
        if($out = (array)app('request')->get('out')){
            $query = $query->whereNotIn('id', $out);
        }
        return $query;
    }
}
