<?php

return [
    'posts.index',
    'posts.create',
    'posts.update',
    'posts.updateMy',
    'posts.delete',
    'posts.deleteMy',

    'pages.index',
    'pages.create',
    'pages.update',
    'pages.delete',

    'categories.index',
    'categories.create',
    'categories.update',
    'categories.delete',

//    'taxonomies.index',
//    'taxonomies.create',
//    'taxonomies.update',
//    'taxonomies.delete',

    'tags.index',
    'tags.create',
    'tags.update',
    'tags.delete',

    'images.create',
    'images.update',
    'images.delete',

    'settings.update',
];
