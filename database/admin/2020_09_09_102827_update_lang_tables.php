<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;

class UpdateLangTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('lang')->nullable();
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->string('lang')->nullable();
        });
        Schema::create('langs', function (Blueprint $table) {
            $table->id();
            $table->morphs('langable');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->text('content')->nullable();
            $table->string('lang')->nullable();
            $table->timestampsTz();
        });

        \Illuminate\Support\Facades\DB::table('settings')->
        whereNotIn('name', \PK\Facades\Settings::provider()->excludeLang)->update(['lang' => App::getLocale()]);

        foreach(['post', 'page'] as $instance){
            \Illuminate\Support\Facades\DB::table($instance.'s')->orderBy('id')
                ->chunk(200, function ($items) use ($instance){
                $langs = [];
                foreach($items as $item){
                    $langs[] = [
                        'langable_id' => $item->id,
                        'langable_type' => 'PK\Models\\'.\Illuminate\Support\Str::studly($instance),
                        'title' => $item->title,
                        'description' => $item->description,
                        'content' => $item->content,
                        'lang' => App::getLocale(),
                    ];
                }

                \Illuminate\Support\Facades\DB::table('langs')->insert($langs);
            });
            Schema::table($instance.'s', function (Blueprint $table) {
                $table->dropColumn(['title', 'description', 'content']);
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        Schema::table('settings', function (Blueprint $table) {
            $table->dropColumn('lang');
        });
        foreach(['post', 'page'] as $instance){
            Schema::table($instance.'s', function (Blueprint $table) {
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->text('content')->nullable();
            });
            \Illuminate\Support\Facades\DB::table('langs')
                ->where(['langable_type' => 'PK\Models\\'.\Illuminate\Support\Str::studly($instance)])
                ->orderBy('id')
                ->chunk(200, function ($items) use ($instance){
                    $class = 'PK\Models\\'.\Illuminate\Support\Str::studly($instance);

                    foreach($items as $item){
                        if($object = $class::find($item->langable_id)){
                            $object->update([
                                'title' => $item->title,
                                'description' => $item->description,
                                'content' => $item->content,
                            ]);
                        }
                    }
                });
        }
        Schema::dropIfExists('langs');
    }
}
