<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTableRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table->unsignedBigInteger('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
        });

        $id = \Illuminate\Support\Facades\DB::table('roles')->insertGetId([
            'permissions' => json_encode(app()->permissions)
        ]);
        \Illuminate\Support\Facades\DB::table('langs')->insertGetId([
            'title' => 'Администратор',
            'langable_type' => \PK\Models\Role::class,
            'langable_id' => $id,
            'lang' => 'ru'
        ]);

        \Illuminate\Support\Facades\DB::table('langs')->insertGetId([
            'title' => 'Administrator',
            'langable_type' => \PK\Models\Role::class,
            'langable_id' => $id,
            'lang' => 'en'
        ]);
        \Illuminate\Support\Facades\DB::table('langs')->insertGetId([
            'title' => 'Správce',
            'langable_type' => \PK\Models\Role::class,
            'langable_id' => $id,
            'lang' => 'cz'
        ]);
        \Illuminate\Support\Facades\DB::table('users')->update(['role_id' => $id]);

        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('author_id')->nullable();
            $table->foreign('author_id')->references('id')->on('users')->onDelete('set null');

            $table->unsignedBigInteger('editor_id')->nullable();
            $table->foreign('editor_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role')->nullable();
            $table->dropForeign(['role_id']);
            $table->dropColumn(['role_id']);
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['author_id']);
            $table->dropColumn(['author_id']);

            $table->dropForeign(['editor_id']);
            $table->dropColumn(['editor_id']);
        });
    }
}
