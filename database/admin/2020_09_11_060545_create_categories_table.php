<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('slug')->nullable();
            $table->timestampsTz();
        });

        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });

        foreach(config('categories', []) as $category){
            $id = \Illuminate\Support\Facades\DB::table('categories')->insertGetId([
                'slug' => $category['slug']
            ]);
            \Illuminate\Support\Facades\DB::table('langs')->insert([
                'langable_id' => $id,
                'langable_type' => 'PK\Models\Category',
                'title' => $category['title'],
                'description' => $category['description'],
                'content' => ' ',
                'lang' => App::getLocale(),
            ]);
            \Illuminate\Support\Facades\DB::table('posts')
                ->where('category', $category['slug'])
                ->update(['category_id' => $id]);
        }
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('category')->nullable();
        });
        \Illuminate\Support\Facades\DB::table('categories')->get()->each(function ($category){
            \Illuminate\Support\Facades\DB::table('posts')
                ->where('category_id', $category->id)
                ->update(['category' => $category->slug]);
        });
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign(['category_id'])->nullable();
            $table->dropColumn(['category_id'])->nullable();
        });
        Schema::dropIfExists('categories');
    }
}
