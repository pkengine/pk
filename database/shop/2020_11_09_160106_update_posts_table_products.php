<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePostsTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $category = \Illuminate\Support\Facades\DB::table('categories')
            ->where('slug', 'products')
            ->first();
        if($category){
            \Illuminate\Support\Facades\DB::table('posts')
                ->where('category_id', $category->id)
                ->update(['type' => 'product']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $category = \Illuminate\Support\Facades\DB::table('categories')
            ->where('slug', 'products')
            ->first();
        if($category){
            \Illuminate\Support\Facades\DB::table('posts')
                ->where('category_id', $category->id)
                ->update(['type' => 'post']);
        }
    }
}
