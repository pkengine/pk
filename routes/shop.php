<?php

Route::middleware('web')
    ->namespace('PK\Controllers')
    ->group(function (){
        Route::prefix('order')
            ->namespace('App')
            ->as('order.')
            ->group(function (){
                Route::post('/', 'OrderController@store');
                Route::get('/', 'OrderController@current');
            });
        Route::prefix('cart')
            ->namespace('App')
            ->as('cart.')
            ->group(function (){
                Route::get('/', 'CartController@index');
                Route::post('add/{product}', 'CartController@addItem')->name('product.add');
                Route::post('sub/{product}', 'CartController@subItem')->name('product.sub');
                Route::post('remove/{product}', 'CartController@removeItem')->name('cart.product.remove');
            });

        Route::prefix('admin')
            ->namespace('Admin')
            ->middleware(['auth', \PK\Middleware\UserSettings::class])
            ->as('admin.')
            ->group(function (){
                Route::get('products/make', 'ProductController@make');
                Route::resource('products', 'ProductController', ['parameters' => ['products' => 'post'], 'type' => 'product']);

                Route::get('orders/statuses', 'OrderController@statuses');
                Route::resource('orders', 'OrderController');
            });
    });
