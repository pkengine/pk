<?php
Route::middleware('web')
    ->namespace('PK\Controllers')
    ->group(function (){
        Route::namespace('Auth')->group(function (){
            Route::get('login', 'LoginController@loginForm')->middleware('guest')->name('login.form');
            Route::post('login', 'LoginController@login')->middleware('guest');
            Route::post('logout', 'LoginController@logout')->middleware('auth');
            Route::post('recovery-password', 'ResetPasswordController@recoveryPassword')->middleware('guest')->name('password.recovery');
            Route::get('reset-password', 'ResetPasswordController@resetPasswordForm')->middleware('guest')->name('password.reset');
            Route::post('reset-password', 'ResetPasswordController@resetPassword')->middleware('guest');
        });

        Route::namespace('App')
            ->as('app.')
            ->group(function (){
                Route::post('lang/{lang}', 'LangController@set');
                Route::get('sitemap.xml', 'SitemapController@get');
                Route::get('sitemap-{instance}.xml', 'SitemapController@get')->name('sitemap');
            });
        Route::prefix('admin')
            ->namespace('Admin')
            ->middleware(['auth', \PK\Middleware\UserSettings::class])
            ->as('admin.')
            ->group(function (){
                Route::get('/', 'IndexController@index')->name('index');
                Route::get('/data', 'IndexController@data');
                Route::put('account', 'UserController@account')->name('account.update');
                Route::get('settings', 'SettingController@index')->name('settings.index');
                Route::get('settings/edit', 'SettingController@edit')->name('settings.edit');
                Route::put('settings', 'SettingController@update')->name('settings.update');

                Route::get('instances', 'InstanceController@index');

                Route::get('categories/tree', 'CategoryController@tree');

                Route::get('posts/make', 'PostController@make');
                Route::get('categories/make', 'CategoryController@make');
                Route::get('tags/make', 'TagController@make');
                Route::get('taxonomies/make', 'TaxonomyController@make');
                Route::get('pages/make', 'PageController@make');
                Route::get('roles/make', 'RoleController@make');

                Route::resource('pages', 'PageController');
                Route::post('posts/{post}/restore', 'PostController@restoreTrash');
                Route::resource('posts', 'PostController');
                Route::resource('categories', 'CategoryController');
                Route::post('categories/{category}/sort/{type}', 'CategoryController@sort');
                Route::resource('tags', 'TagController');
                Route::resource('taxonomies', 'TaxonomyController');
                Route::resource('users', 'UserController');
                Route::resource('images', 'ImageController');
                Route::get('roles/permissions', 'RoleController@permissions');
                Route::resource('roles', 'RoleController');
            });
    });

